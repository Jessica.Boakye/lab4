package cellular;
import datastructure.CellGrid;

import datastructure.IGrid;
import java.util.Random;

public class BriansBrain implements CellAutomaton{
    IGrid currentGeneration;

    public BriansBrain(int rows, int columns){
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }


    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random ();
        for (int row = 0; row < currentGeneration.numRows(); row++){
            for (int col = 0; col < currentGeneration.numColumns(); col++){
            
            if (random.nextBoolean()){
                currentGeneration.set(row, col, CellState.ALIVE);
            }
            else {
                currentGeneration.set(row, col, CellState.DEAD);
            }

        }
        
    }
}

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int i = 0; i < getGrid().numRows(); i++){
            for (int j = 0; j < getGrid().numColumns(); j++){
                nextGeneration.set(i,j,getNextCell(i,j));
            }
        }
       this.currentGeneration = nextGeneration; 
    }

    @Override
    public CellState getNextCell(int row, int col) {
        if(getCellState(row, col) == CellState.ALIVE){
            return CellState.DYING;
        }
        else if (getCellState(row, col) == CellState.DYING){
            return CellState.DEAD;
        }
        else if (getCellState(row, col)== CellState.DEAD){
            if (countNeighbors(row, col, CellState.ALIVE) == 2){
                return CellState.ALIVE;
            }
        }
        return CellState.DEAD;
    }
    private int countNeighbors (int row, int col, CellState state){
        int count = 0;
        for (int rowNum = row - 1 ; rowNum <= (col + 1) ; rowNum +=1  ) {
            for (int colNum = col - 1 ; colNum <= (row + 1) ; colNum +=1  ) {
                if(! ((rowNum == row) && (colNum == col))) {
                    if((colNum >= 0) && (rowNum >= 0) ) {
                        if((rowNum <= currentGeneration.numRows()) && (colNum <= currentGeneration.numColumns())) {
                            if(currentGeneration.get(rowNum, colNum) == state){
                                count ++;
                            }
                        }
                    }
                }
            }
        }
        return count;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
    
}
